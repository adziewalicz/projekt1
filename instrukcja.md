**Spis treści**
[toc] 

## **Wstęp**

Obecnie powszechnie wykorzystuje się języki ze znacznikami do opisania dodatkowych informacji

umieszczanych w plikach tekstowych. Z pośród najbardziej popularnych można wspomnieć o:

1. **html** – służącym do opisu struktury informacji zawartych na stronach internetowych,

2. **Tex** (Latex) – poznany na zajęciach język do „profesjonalnego” składania tekstów,

3. **XML** (Extensible Markup Language) - uniwersalnym języku znaczników przeznaczonym

do reprezentowania różnych danych w ustrukturalizowany sposób.

Przykład kodu *html* i jego interpretacja w przeglądarce:

```html
<!DOCTYPE **html**>

<**html**>

<**head**>

<**meta** charset="utf-8" />

<title>Przykład</title>

</**head**>

<**body**>

<**p**> Jakiś paragraf tekstu</**p**>

</**body**>

</**html**>
```

Przykład kodu *Latex* i wygenerowanego pliku w formacie *pdf*

```latex
\documentclass[]{letter}

\usepackage{lipsum}

\usepackage{polyglossia}

\setmainlanguage{polish}

\**begin**{**document**}

\**begin**{**letter**}{**Szanowny Panie XY**}

\address{Adres do korespondencji}

\opening{}

\lipsum[2]

\signature{Nadawca}

\closing{Pozdrawiam}

\**end**{**letter**}

\**end**{**document**}
```

Przykład kodu *XML* – fragment dokumentu *SVG* (Scalar Vector Graphics)

```xml
<!DOCTYPE **html**>

<**html**>

<**body**>

<svg height="100" width="100">

<circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />

</svg>

</**body**>

</**html**>
```

W tym przypadku mamy np. znacznik np. *<circle>* opisujący parametry koła i który może być

właściwie zinterpretowany przez dedykowaną aplikację (np. przeglądarki www).

Jako ciekawostkę można podać fakt, że również pakiet MS Office wykorzystuje format XML do

przechowywania informacji o dodatkowych parametrach formatowania danych. Na przykład pliki z

rozszerzeniem *docx*, to nic innego jak spakowane algorytmem zip katalogi z plikami xml.

Przykład rozpakowania zawartości pliku *test.docx* poleceniem: **unzip**

```
$unzip -l test.docx

Archive: test.docx

Length Date Time Name

\--------- ---------- ----- ----

573 2022-03-20 08:55 \_rels/.rels

731 2022-03-20 08:55 docProps/core.xml

508 2022-03-20 08:55 docProps/app.xml

531 2022-03-20 08:55 word/\_rels/document.xml.rels

1288 2022-03-20 08:55 word/document.xml

2429 2022-03-20 08:55 word/styles.xml

853 2022-03-20 08:55 word/fontTable.xml

257 2022-03-20 08:55 word/settings.xml

1374 2022-03-20 08:55 [Content\_Types].xml
```

Wszystkie te języki znaczników cechują się rozbudowaną i złożoną składnią i dlatego do ich edycji

wymagają najczęściej dedykowanych narzędzi w postaci specjalizowanych edytorów. By

wyeliminować powyższą niedogodność powstał **Markdown** - uproszczony język znaczników

służący do formatowania dokumentów tekstowych (bez konieczności używania specjalizowanych

narzędzi). Dokumenty w tym formacie można bardzo łatwo konwertować do wielu innych

formatów: np. html, pdf, ps (postscript), epub, xml i wiele innych. Format ten jest powszechnie

używany do tworzenia plików README.md (w projektach open source) i powszechnie

obsługiwany przez serwery git’a. Język ten został stworzony w 2004 r. a jego twórcami byli John

Gruber i Aaron Swartz. W kolejnych latach podjęto prace w celu stworzenia standardu rozwiązania

i tak w 2016 r. opublikowano dokument [RFC](https://tools.ietf.org/html/rfc7764) który zawiera opis kilku odmian tegoż języka:

* CommonMark,

* GitHub Flavored Markdown (GFM),

* Markdown Extra.

## **Podstawy składni**

Podany link: <https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet>[ ](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)zawiera opis

podstawowych elementów składni w języku angielskim. Poniżej zostanie przedstawiony ich krótki

opis w języku polskim.

### **Definiowanie nagłówków**

W tym celu używamy znaku kratki

Lewe okno zawiera kod źródłowy – prawe -podgląd przetworzonego tekstu

### **Definiowanie list**

Listy numerowane definiujemy wstawiając numery kolejnych pozycji zakończone kropką.

Listy nienumerowane definiujemy znakami: \*,+,-

### **Wyróżnianie tekstu**

### **Tabele**

Centrowanie zawartości kolumn realizowane jest poprzez odpowiednie użycie znaku dwukropka:

### **Odnośniki do zasobów**

```markdown
[odnośnik do zasobów]([www.gazeta.pl](http://www.gazeta.pl/))

[odnośnik do pliku](LICENSE.md)

[odnośnik do kolejnego zasobu][1]

[1]: http://google,com
```

### **Obrazki**

```markdown
![alt text](https://server.com/images/icon48.png "Logo 1") – obrazek z zasobów

internetowych

![](logo.png) – obraz z lokalnych zasobów
```

### **Kod źródłowy dla różnych języków programowania**

### **Tworzenie spisu treści na podstawie nagłówków**

## **Edytory dedykowane**

Pracę nad dokumentami w formacie Markdown( rozszerzenie md) można wykonywać w

dowolnym edytorze tekstowym. Aczkolwiek istnieje wiele dedykowanych narzędzi

1. marktext - https://github.com/marktext/marktext

2. <https://hackmd.io/>[ ](https://hackmd.io/)online editor

3. Visual Studio Code z wtyczką „markdown preview”

## **Pandoc – system do konwersji dokumentów Markdown do innych formatów**

Jest oprogramowanie typu open source służące do konwertowania dokumentów

pomiędzy różnymi formatami.

Pod poniższym linkiem można obejrzeć przykłady użycia:

<https://pandoc.org/demos.html>

Oprogramowanie to można pobrać z spod adresu: <https://pandoc.org/installing.html>

Jeżeli chcemy konwertować do formatu latex i pdf trzeba doinstalować oprogramowanie

składu Latex (np. Na windows najlepiej sprawdzi się Miktex https://miktex.org/)

Gdyby podczas konwersji do formatu pdf pojawił się komunikat o niemożliwości

znalezienia programu pdflatex rozwiązaniem jest wskazanie w zmiennej środowiskowej

PATH miejsca jego położenia

Pod adresem (https://gitlab.com/mniewins66/templatemn.git) znajduje się przykładowy plik

Markdown z którego można wygenerować prezentację w formacie pdf wykorzystując

klasę latexa beamer.

W tym celu należy wydać polecenie z poziomu terminala:

```
$pandoc templateMN.md -t beamer -o prezentacja.pdf
```

# **Lekcja 2 – Git – system kontroli wersji**

Jest to oprogramowanie służące do śledzenia zmian (głównie w dokumentach tekstowych)

podczas pracy zespołów wieloosobowych dokonujących zmian w różnym czasie i

miejscach przebywania.

## **Git - podstawowe cechy**

+ efektywna praca z dużymi projektami - jest jednym z najszybszych systemów

kontroli wersji

+ wsparcie dla protokołów sieciowych - dane można wymieniać przez HTTP(S), FTP,

rsync, SSH, e-mail

+ każda kopia repozytorium to obraz całego projektu - Git nie zapamiętuje zmian

między kolejnymi rewizjami lecz kompletne obrazy (snapshots)

+ możliwość tworzenia oprogramowania z rozgałęzieniami

+ tryb pracy off-line - każdy pracuje na własnej kopii repozytorium, a następnie

zmiany mogą być wymieniane między lokalnymi repozytoriami jak również

serwerem.

## **Idea pracy:**

Na powyższym rysunku symbolicznie przedstawiono zasadę pracy z gitem. Większy okrąg symbolizuje repozytorium lokalne (po lewej) w którym pracuje użytkownik. Mniejszy okrąg (po prawej) to kopia repozytorium na wybranym serwerze (np. github.com lub gitlab.com). Czerwone strzałki to komendy git’a, które powodują przenoszenie plików i/lub katalogów pomiędzy poszczególnymi elementami repozytorium lub pomiędzy maszyną lokalną a serwerem.

Git rozróżnia trzy typy plików w repozytorium lokalnym: nadzorowane, pomijane i nienadzorowane. Z punku widzenia systemu plików repozytorium to zwykły katalog w którym zaicjalizowany został specjalny podkatalog o nazwie .git, w którym przetrzymywane są wszystkie informacje o repozytorium.

Użytkownik pracuje w katalogu roboczym, gdzie modyfikuje swoje pliki. Gdy uzna, że postęp prac wymaga zapisania zmian przenosi je do *staging area (*komenda add). Gdy suma zmian stanowi jakąś spójną całość (np. dodanie nowego rozdziału pracy, dodanie nowej funkcjonalności itd.) tworzy się nową „migawkę” zapisywaną w lokalnym repozytorium (komenda commit). Co jakiś czas można przenieść lokalne zmiany w repozytorium na serwer (komenda push).

Uwaga: Do pracy z systemem git należy zainstalować oprogramowanie ze strony:

<https://git-scm.com/downloads>

Po zainstalowaniu będzie dostępna powłoka z wyglądu podobna do tej z poniższego rysunku

Jest to aplikacja **Git Bash** w której wydajemy polecenia z linii komend.

Pierwszym krokiem po instalacji git’a i uruchomieniu aplikacji Git Bash jest wydanie dwóch

poleceń:

```
**$git config --global user.name "Student Wspaniały"**

**$git config --global user.email "wspanialy@pw.edu.pl"**
```

W ten sposób informujemy system git kto będzie autorem zmian wprowadzanych do repozytorium

Uwaga: należy wprowadzić własne dane osobowe i własny adres e-mail

W przypadku piszącego tą instrukcję wynik wprowadzonych komend ma postać:

## **Git – tworzenie pustego archiwum lokalnego.**

Należy w konsoli wywołać następującą sekwencję komend (zakładając, że repozytorium będzie

nosiło nazwę repo1):

1.```$mkdir repo1```

2.```$cd repo1```

3.```$git init```

4.```$git status```

Pierwsza komenda tworzy katalog o zadanej nazwie.

Druga komenda powoduje, że przechodzimy do właśnie stworzonego katalogu

Trzecia komenda inicjuje puste repozytorium

Czwarta komenda wyświetla informacje o stanie repozytorium

Jak widać na powyższym rysunku po stworzeniu pustego repozytorium pracujemy w głównej gałęzi **master** i brak jest zarejestrowanych jakichkolwiek „migawek” (commits)

Dodawanie pliku/ów do indeksu:

* ```$git add file``` - dodanie pliku do indeksu

* ```$git rm - -cached file``` - usunięcie pliku z indeksu

Powyższy rysunek prezentuje sekwencję komend:

1. ``` $touch.exe nowy.md``` – utworzenie pustego pliku tekstowego

2. ``` $git add nowy.md``` – dodanie nowo stworzonego pliku do indeksu

3. ``` $git status``` – wyświetlenie statu lokalnego repozytorium

Widać (zielony kolor), że git śledzi wprowadzone zmiany

Git - zapis pliku do repozytorium lokalnego:

* ```$git commit -m "komunikat"``` – w komunikacie podaje się krotki opis zmian wprowadzonych do repozytorium

Polecenie ```$git log``` wyświetla listę zapisanych „migawek”. Każda migawka identyfikowana jest sekwencją liczb szesnastkowych (żółte cyfry) – komentarzem jaki podajemy podczas wywoływania polecenia ```$git commit```.

**Git – dodanie kolejnych plików, modyfikacja i stworzenie kolejnych „migawek”**

Używając tekstowego edytora nano (można użyć własnego ulubionego edytora) – zmodyfikowano zawartość pliku nowy.md – co natychmiast zauważył system kontroli wersji (modified nowy.md). Następnie stworzono kolejny pusty plik (następny.md) co również system zasygnalizował jako: (Untracked files – następny.md). Teraz zostaną wykonane dwa niezależne commity i zostanie wyświetlony kolejny log stanu repozytorium.

Jak widać na kolejnym rysunku w repozytorium zostały umieszczone trzy „migawki” stanu repozytorium wszystkie umieszczone w gałęzi głównej „master”.

**Git – praca z rozgałęzieniami**

Bardzo często zachodzi taka sytuacja, że nie chcemy wprowadzać zmian w głównej gałęzi a tylko wykonać jakieś prace testowe i później zdecydować czy dołączyć je do głównej gałęzi lub nie. Do tego służą rozgałęzienia, i operacja ich łączenia.

* ```$git branch name``` – tworzenie nowego rozgałęzienia

* ```$git checkout name``` - przełączenie się do innej gałęzi

* ```$git branch -D name``` - usunięcie rozgałęzienia (trzeba być od niego odłączonym)

* ```$ git merge nazwa\_gałęzi``` – złączenie gałęzi „nazwa\_gałęzi” z gałęzią do której jesteśmy podłączeni

Każde lokalne repozytorium możemy umieścić na dedykowanym serwerze protokołu git (możemy także spakować katalog z repozytorium i taką kopię przesłać zainteresowanemu np. E-mailem).

Jeżeli chcemy realizować wspólne projekty - nad którymi pracuje wiele osób - to niezbędnym jest wykorzystanie serwera.

Mamy do dyspozycji np.

* GitHub - [https://github.com](https://github.com/)

* GitLab - https://gitlab.com

* GitLab Wydziałowy - [https://gitlab-stud.elka.pw.edu.pl](https://gitlab-stud.elka.pw.edu.pl/)[ ](https://gitlab-stud.elka.pw.edu.pl/)(do niego możecie nie mieć Państwo nadanych praw dostępu)

Sugeruje się wykorzystanie serwera gitlab z uwagi na łatwość obsługi.

W tym celu należy wykonać następujące kroki.

1. Zarejestrować się w systemie

2. Stworzyć nowy projekt w którym będziemy chcieli umieścić nasze lokalne repozytorium

3. W lokalnym repozytorium dodać informacje na jaki serwer mają być wysyłane dane

realizuje to komenda:

``` **$git remote add origin <git@gitlab.com>:user/projekt1.git**```

(jako user nazwa Państwa konta na serwerze)

I teraz można „wypchnąć” lokalne repozytorium na serwer komendą:

``` **$git push origin master**```

gdy istnieją dodatkowe rozgałęzienia w repozytorium, to każde musi być „wypchnięte” oddzielną

komendą

```**$git push origin nazwaGalezi**```

Gdy chcemy pobrać repozytorium z serwera na jakąś inną lokalną maszynę to wtedy należy

wykonać klonowanie zdalnego repozytorium komendą:

```**$git clone git@gitlab.com:user/projekt1.git**```

Na potrzeby tej instrukcji stworzono projekt o nazwie testowy na koncie gitlab autora instrukcji

Po wciśnięciu przycisku **Create project** zostanie wygenerowany ekran z listą instrukcji które należy wykonać w zależności od kontekstu w jakim chcemy użyć nasz projekt.

  Ostatni rysunek pokazuje przesłanie lokalnego repozytorium na serwer gitlab.com

W tym przypadku nie musiano podawać loginu i hasła ponieważ pomiędzy maszyną lokalną a serwerem są uzgodnione pary kluczy: prywatny i publiczn
  

  
  